set nocompatible              " be iMproved, required
filetype off                  " required
syntax on


call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'

call plug#end()

