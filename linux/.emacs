(add-to-list 'load-path "~/.emacs.d/elpa/")
(add-to-list 'load-path "~/.emacs.d/elpa/calmer-forest-theme-20130925.2210/")

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

(menu-bar-mode 0)
(tool-bar-mode 0)
(set-scroll-bar-mode nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-directory-alist (quote ((".*" . "~/emacsBackup"))))
 '(custom-safe-themes
   (quote
    ("7997e0765add4bfcdecb5ac3ee7f64bbb03018fb1ac5597c64ccca8c88b1262f" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(set-face-attribute 'default nil :height 120)

(require 'calmer-forest-theme)
(load-theme 'calmer-forest t)


;;; hooks
(defun c++-mode-hook ()
  (c-set-style "stroustrup")
  (setq c-basic-offset 2)
  (setq c-auto-newline nil)
  (setq indent-tabs-mode nil)
  (show-paren-mode t)
  (c-subword-mode)
  )

(defun c-mode-hook ()
  (c-set-style "stroustrup")
  (setq c-basic-offset 2)
  (setq c-auto-newline nil)
  (setq indent-tabs-mode nil)
  (show-paren-mode t)
  (c-subword-mode)
  )

(setq c-mode-hook 'c-mode-hook)
(setq c++-mode-hook 'c++-mode-hook)
(add-hook 'c-mode-common-hook 'turn-on-font-lock)

(add-hook 'python-mode-hook
 (lambda ()
   (setq indent-tabs-mode t)
   (setq python-indent 4)
   (setq tab-width 4)))

;; not working yet!
(defun python-env ()
 (message "test")
 (split-window-below)
 (split-window-right))
