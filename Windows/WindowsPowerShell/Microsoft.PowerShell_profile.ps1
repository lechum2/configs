# set regular console colors
[console]::backgroundcolor = "black"
[console]::foregroundcolor = "darkgreen"
# set special colors
$p = $host.privatedata
$p.ErrorForegroundColor    = "Red"
$p.ErrorBackgroundColor    = "Black"
$p.WarningForegroundColor  = "Yellow"
$p.WarningBackgroundColor  = "Black"
$p.DebugForegroundColor    = "Yellow"
$p.DebugBackgroundColor    = "Black"
$p.VerboseForegroundColor  = "Yellow"
$p.VerboseBackgroundColor  = "Black"
$p.ProgressForegroundColor = "Yellow"
$p.ProgressBackgroundColor = "DarkCyan"
function which($name)
{
    Get-Command $name | Select-Object -ExpandProperty Definition
}

set-location E:\workspace
# clear screen
clear-host

#poshgit from choco
if(Test-Path Function:\Prompt) {Rename-Item Function:\Prompt PrePoshGitPrompt -Force}

# Load posh-git example profile
. 'C:\tools\poshgit\dahlbyk-posh-git-19f3f2e\profile.example.ps1'

Rename-Item Function:\Prompt PoshGitPrompt -Force
function Prompt() {if(Test-Path Function:\PrePoshGitPrompt){++$global:poshScope; New-Item function:\script:Write-host -value "param([object] `$object, `$backgroundColor, `$foregroundColor, [switch] `$nonewline) " -Force | Out-Null;$private:p = PrePoshGitPrompt; if(--$global:poshScope -eq 0) {Remove-Item function:\Write-Host -Force}}PoshGitPrompt}
